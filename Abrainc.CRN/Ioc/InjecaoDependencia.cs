﻿using Abrainc.Repositorio.Posts;
using Microsoft.Extensions.DependencyInjection;

namespace Abrainc.Repositorio.Ioc
{
    public static class InjecaoDependencia
    {
        public static void RegistrarDependencias(IServiceCollection services)
        {
            services.AddScoped<IPostRepository, PostRepository>();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abrainc.Repositorio.DataAcess

{
    public interface IDataAcess
    {
        Task Add<T>(T entity) where T : class;
        Task Atualiza<T>(T entity) where T : class;
        Task Deleta<T>(T entity) where T : class;
        Task<bool> SalvarAsync();
    }
}

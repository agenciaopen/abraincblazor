using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Abrainc.Data.Model.AbraincArtigo;
using Abrainc.Repositorio.Context;
using Microsoft.Extensions.Configuration;

namespace Abrainc.CRN.ArtigosAcademicos
{
    public class ArtigoAcademicoRepository : IArtigoAcademicoRepository
    {
        private readonly AppDbContextCRN appContext;
        public ArtigoAcademicoRepository(AppDbContextCRN configuration) => appContext = configuration;

        public bool Add(ArtigoAcademico artigoAcademico)
        {
            appContext.Add(artigoAcademico);
            return appContext.SaveChanges() > 0;
        }

        public bool Delete(ArtigoAcademico artigoAcademico)
        {
            appContext.Remove(artigoAcademico);
            return appContext.SaveChanges() > 0;
        }

        public List<ArtigoAcademico> GetAll() => appContext.ArtigosAcademicos.ToList();

         public ArtigoAcademico GetById(int id) => appContext.ArtigosAcademicos.Find(id);

        public bool Update(ArtigoAcademico artigoAcademico)
        {
            appContext.Update(artigoAcademico);
            return appContext.SaveChanges() > 0;
        }
    }
}

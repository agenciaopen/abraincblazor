using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abrainc.Data.Model.AbraincArtigo;
using Microsoft.Extensions.Configuration;

namespace Abrainc.CRN.ArtigosAcademicos
{
    public interface IArtigoAcademicoRepository
    {
        bool Add(ArtigoAcademico artigoAcademico);
        bool Delete(ArtigoAcademico artigoAcademico);
        List<ArtigoAcademico> GetAll();
        ArtigoAcademico GetById(int id);
        bool Update(ArtigoAcademico artigoAcademico);
    }
}
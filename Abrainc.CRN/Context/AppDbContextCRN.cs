﻿using Abrainc.Data.Model.AbraincArtigo;
using Abrainc.Data.Model.Universidades;
using Abrainc.Dominio.Model.AbraincSocial;
using Abrainc.Dominio.Model.Associadas;
using Abrainc.Dominio.Model.Componentes.Associar;
using Abrainc.Dominio.Model.Componentes.Banner;
using Abrainc.Dominio.Model.ConfiguracoesDoSistema;
using Abrainc.Dominio.Model.Evento;
using Abrainc.Dominio.Model.Formulario;
using Abrainc.Dominio.Model.Galeria;
using Abrainc.Dominio.Model.Identity;
using Abrainc.Dominio.Model.Lab;
using Abrainc.Dominio.Model.Paginas.QuemSomos;
using Abrainc.Dominio.Model.Posts;
using Abrainc.Dominio.Model.Seo;
using Abrainc.Dominio.Model.TermosAceite;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Abrainc.Repositorio.Context
{
    public class AppDbContextCRN : IdentityDbContext
    {
        public AppDbContextCRN(DbContextOptions<AppDbContextCRN> options)
           : base(options)
        {
        }

        #region POSTS/CATEGORIAS
        public DbSet<Post> Posts { get; set; }
        public DbSet<Categoria> Categorias { get; set; }
        public DbSet<AutorArtigo> AutorArtigos { get; set; }
        public DbSet<PodCast> PodCasts { get; set; }
        public DbSet<PostGaleria> PostGalerias { get; set; }
        #endregion

        #region SEO
        public DbSet<SeoGoogle> SeoGoogles { get; set; }
        public DbSet<SeoRedesSociais> SeoRedesSociais { get; set; }
        #endregion


        #region QUEMSOMOS
        public DbSet<Comite> Comites { get; set; }
        public DbSet<Gestao> Gestaos { get; set; }
        public DbSet<GestaoGaleria> GestaoGaleria { get; set; }
        public DbSet<QuemSomos> QuemSomosP { get; set; }


        #endregion

        public DbSet<IpUsuario> IpUsuario { get; set; }
        public DbSet<GaleriaImagem> GaleriaImagens { get; set; }
        public DbSet<LogosTopo> LogosTopo { get; set; }


        #region LAB
        public DbSet<Indicador> Indicadores { get; set; }
        public DbSet<IndicadorTipo> IndicadorTipos { get; set; }
        public DbSet<Periodo> Periodos { get; set; }
        public DbSet<Sessao> Sessaos { get; set; }
        public DbSet<TipoRelatorio> TipoRelatorios { get; set; }
        #endregion
        #region USER
        public DbSet<UsuarioProfissao> UsuarioProfissoes { get; set; }

        #endregion
        #region FORMULARIO
        public DbSet<GravaFormulario> GravaFormularios { get; set; }
        public DbSet<TipoFormulario> TipoFormularios { get; set; }
        #endregion
        #region EVENTOS
        //public DbSet<Evento> Eventos { get; set; }
        public DbSet<TipoDeApoioPost> TipoDeApoios { get; set; }

        #endregion
        #region CONFIGURACOES DO SISTEMA

        public DbSet<Administrador> Administradores { get; set; }
        public DbSet<LGPD> LGPDS { get; set; }
        public DbSet<Menu> Menus { get; set; }
        public DbSet<Paginacao> Paginacoes { get; set; }
        public DbSet<ServidorEmail> ServidorEmails { get; set; }

        #endregion


        #region COMPONENTS
        #region BANNER
        public DbSet<HeroSection> HeroSections { get; set; }
        public DbSet<BannerGaleria> BannerGalerias { get; set; }
        public DbSet<BannerPromocional> BannerPromocionals { get; set; }
        public DbSet<BannerPromocionalGaleria> BannerPromocionalGalerias { get; set; }

        #endregion

        #region Associar
        // public DbSet<AssociaSe> Associa { get; set; }
        public DbSet<AssociaSeGaleria> AssociaSeGalerias { get; set; }
        public DbSet<AssociaSePrincipaisAtividade> AssociaSePrincipaisAtividades { get; set; }
        public DbSet<AssociaSePrincipaisAtividadesGaleria> AssociaSePrincipaisAtividadesGalerias { get; set; }

        #endregion
        #endregion



        #region Associadas
        public DbSet<Associada> Associadas { get; set; }
        public DbSet<AssociaSe> Associes { get; set; }
        #endregion


        #region ArtigosAcademicos

        public DbSet<ArtigoAcademico> ArtigosAcademicos { get; set; }
        public DbSet<Autor> Autores { get; set; }
        public DbSet<Universidade> Universidades { get; set; }

        #endregion
    }
}

﻿using Abrainc.Dominio.Model.Identity;

namespace Abrainc.Repositorio.Usuario;
public interface IUserServiceRepository
{
    Task<List<User>> GetUsers();
    Task<User> GetUser(Guid id);
    Task<bool> DeleteUser(Guid id);
    Task<bool> UpdateUserRole(Guid id, User user);
}


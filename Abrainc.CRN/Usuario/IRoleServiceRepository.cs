﻿using Abrainc.Dominio.Model.Identity;

namespace Abrainc.Repositorio.Usuario;
public interface IRoleServiceRepository
{
    Task<List<Role>> GetRoles();
    Task<List<Role>> GetRolesUser(Guid id);
    Task<bool> CreateRole(Role role);
    Task<Role> GetRole(Guid id);
    Task<bool> EditRole(Guid id, Role role);
    Task<bool> DeleteRole(Guid id);
}

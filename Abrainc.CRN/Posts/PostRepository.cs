﻿using Abrainc.Dominio.Model.Posts;
using Abrainc.Dominio.Model.Seo;
using Abrainc.Repositorio.Context;
using System.Linq;

namespace Abrainc.Repositorio.Posts
{
    public class PostRepository : IPostRepository
    {
        private readonly AppDbContextCRN appContext;

        public PostRepository(AppDbContextCRN configuration)
        {
            appContext = configuration;
        }

        public async Task<int> Add(Post post)
        {
            await appContext.AddAsync(post);
            await appContext.SaveChangesAsync();
            await Inicializa(post, post.Id);
            return post.Id;
        }

        public Post BuscaPostPorId(int id)
        {
            var postGet = appContext.Posts.Where(x => x.Id == id).FirstOrDefault();
            return postGet;
        }

        public IEnumerable<Post> BuscaTodosPosts()
        {
           return appContext.Posts.ToList();
        }

        public async Task<bool> Inicializa(Post post, int Id)
        {
            //int res = await Add(post);
            if (Id > 0)
            {
                // Inicializa Redes Sociais
                return await VinculaSeo(Id, post) > 0;
            }
            return false;
        }

        public async Task<int> VinculaSeo(int idPost, Post post)
        {
            SeoGoogle seoGoogle = new()
            {
                Description = "",
                Keyword = "",
                Title = ""
            };
            SeoRedesSociais seoRedes = new()
            {
                Description = "",
                Keyword = "",
                Title = "",
                Image = ""
            };

            appContext.Add(seoGoogle);
            appContext.Add(seoRedes);

            await appContext.SaveChangesAsync();
            post.SeoRedesSociaisId = seoRedes.Id;
            post.SeoGoogleId = seoGoogle.Id;
            return await appContext.SaveChangesAsync();
        }


    }
}




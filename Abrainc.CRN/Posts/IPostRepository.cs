﻿using Abrainc.Dominio.Model.Posts;

namespace Abrainc.Repositorio.Posts
{
    public interface IPostRepository
    {
        Task<int> Add(Post post);

        Task<bool> Inicializa(Post post, int Id);

        Task<int> VinculaSeo(int Id, Post post);

        Post BuscaPostPorId(int id);

        IEnumerable<Post> BuscaTodosPosts();
    }
}

using Abrainc.Data.Model.Universidades;
using Abrainc.Repositorio.Context;

namespace Abrainc.CRN.Universidades
{
    public class UniversidadeRepository : IUniversidadeRepository
    {
        private readonly AppDbContextCRN appContext;
        public UniversidadeRepository(AppDbContextCRN configuration) => appContext = configuration;

        public bool Add(Universidade universidade)
        {
            appContext.Add(universidade);
            return appContext.SaveChanges() > 0;
        }

        public bool Delete(Universidade universidade)
        {
            appContext.Remove(universidade);
            return appContext.SaveChanges() > 0;
        }

        public List<Universidade> GetAll() => appContext.Universidades.ToList();

         public Universidade GetById(int id) => appContext.Universidades.Find(id);

        public bool Update(Universidade universidade)
        {
            appContext.Update(universidade);
            return appContext.SaveChanges() > 0;
        }
    }
}
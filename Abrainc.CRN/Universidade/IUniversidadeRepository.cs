using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abrainc.Data.Model.Universidades;

namespace Abrainc.CRN.Universidades
{
    public interface IUniversidadeRepository
    {
        bool Add(Universidade universidade);
        bool Delete(Universidade universidade);
        List<Universidade> GetAll();
        Universidade GetById(int id);
        bool Update(Universidade universidade);
    }
}
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abrainc.Data.Model.AbraincArtigo;

namespace Abrainc.CRN.Autores
{
    public interface IAutorRepository
    {
        bool Add(Autor autor);
        bool Delete(Autor autor);
        List<Autor> GetAll();
        Autor GetById(int id);
        bool Update(Autor autor);
    }
}
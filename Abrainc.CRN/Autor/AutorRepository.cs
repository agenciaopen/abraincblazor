using Abrainc.Data.Model.AbraincArtigo;
using Abrainc.Repositorio.Context;

namespace Abrainc.CRN.Autores
{
    public class AutorRepository : IAutorRepository
    {
        private readonly AppDbContextCRN appContext;
        public AutorRepository(AppDbContextCRN configuration) => appContext = configuration;

        public bool Add(Autor autor)
        {
            appContext.Add(autor);
            return appContext.SaveChanges() > 0;
        }

        public bool Delete(Autor autor)
        {
            appContext.Remove(autor);
            return appContext.SaveChanges() > 0;
        }

        public List<Autor> GetAll() => appContext.Autores.ToList();

         public Autor GetById(int id) => appContext.Autores.Find(id);

        public bool Update(Autor autor)
        {
            appContext.Update(autor);
            return appContext.SaveChanges() > 0;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abrainc.Dominio.MetodosExtensao
{
    public static class GeradorDeSlug
    {
        public static string CriaSlug(string frase) => frase.Replace(" ", "-");

    }
}

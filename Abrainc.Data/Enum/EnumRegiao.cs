﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abrainc.Dominio.Enum
{
    public enum EnumRegiao
    {
        AssociadasSudeste = 1,
        AssociadasSul = 2,
        AssociadasNordeste = 3,
        AssociadasNorte = 4,
        AssociadasCentroOeste = 5,

    }
}

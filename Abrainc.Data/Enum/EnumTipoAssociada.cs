﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abrainc.Dominio.Enum
{
    public enum EnumTipoAssociada
    {
        ApoioIntitucional = 1,
        Associadas = 2
    }
}

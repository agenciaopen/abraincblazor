﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abrainc.Dominio.Enum
{
    public enum EnumClassificacaoIncorporadora
    {
        Pequino = 1,
        Medio = 2,
        GrandePorte = 3
    }
}

﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Abrainc.Data.Migrations
{
    public partial class Bd1012 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Administradores",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    UserName = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: false),
                    Email = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: false),
                    RoleId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Administradores", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Associadas",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nome = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    UrlDaLogo = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: false),
                    LinkExterno = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    EnumRTipoAssociada = table.Column<int>(type: "int", nullable: false),
                    EnumRegiao = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Associadas", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AssociaSePrincipaisAtividades",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    LinkParaRedirecionar = table.Column<string>(type: "nvarchar(300)", maxLength: 300, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AssociaSePrincipaisAtividades", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Associes",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Incorporadora = table.Column<string>(type: "nvarchar(300)", maxLength: 300, nullable: false),
                    ClassificacaoIncorporadora = table.Column<int>(type: "int", nullable: false),
                    RegiaoAtuacao = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false),
                    NumeroDeLancamentos = table.Column<int>(type: "int", nullable: false),
                    NumeroDeUnidadesEntregue = table.Column<int>(type: "int", nullable: false),
                    Responsavel = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    Cargo = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false),
                    Telefone = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false),
                    Email = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Mensagem = table.Column<string>(type: "nvarchar(300)", maxLength: 300, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Associes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Categorias",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nome = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Slug = table.Column<string>(type: "nvarchar(120)", maxLength: 120, nullable: true),
                    PostId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Categorias", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Comites",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Titulo = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false),
                    Icone = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Comites", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Gestaos",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nome = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    Cargo = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Gestaos", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Indicadores",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nome = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    Slug = table.Column<string>(type: "nvarchar(120)", maxLength: 120, nullable: true),
                    UrlImagemLogo = table.Column<string>(type: "nvarchar(300)", maxLength: 300, nullable: false),
                    IndicadorTiposId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Indicadores", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "IpUsuario",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Ip = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DataRegistro = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IpUsuario", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "LGPDS",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Descricao = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LGPDS", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Menus",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nome = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Menus", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Paginacoes",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    QuantidadeArtigos = table.Column<int>(type: "int", nullable: false),
                    QuantidadeArtigosRelacionado = table.Column<int>(type: "int", nullable: false),
                    QuantidadeNoticias = table.Column<int>(type: "int", nullable: false),
                    QuantidadeNoticiasRelacionadas = table.Column<int>(type: "int", nullable: false),
                    QuantidadeBusca = table.Column<int>(type: "int", nullable: false),
                    QuantidadeSocial = table.Column<int>(type: "int", nullable: false),
                    QuantidadeDadosMercado = table.Column<int>(type: "int", nullable: false),
                    QuantidadeEventos = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Paginacoes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "QuemSomosP",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Titulo = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false),
                    Descricao = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    PrincipiosValoresIcone = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    PrincipiosValoresDescricao = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_QuemSomosP", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SeoGoogles",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(type: "nvarchar(60)", maxLength: 60, nullable: true),
                    Description = table.Column<string>(type: "nvarchar(350)", maxLength: 350, nullable: true),
                    Keyword = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SeoGoogles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SeoRedesSociais",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(type: "nvarchar(60)", maxLength: 60, nullable: true),
                    Description = table.Column<string>(type: "nvarchar(350)", maxLength: 350, nullable: true),
                    Keyword = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    Image = table.Column<string>(type: "nvarchar(250)", maxLength: 250, nullable: true),
                    Type = table.Column<string>(type: "nvarchar(10)", maxLength: 10, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SeoRedesSociais", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ServidorEmails",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Host = table.Column<string>(type: "nvarchar(300)", maxLength: 300, nullable: false),
                    Passaword = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false),
                    EmailSmtp = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Porta = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ServidorEmails", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Sessaos",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nome = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Slug = table.Column<string>(type: "nvarchar(120)", maxLength: 120, nullable: true),
                    TipoRelatorioId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sessaos", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TipoDeApoios",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Descricao = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TipoDeApoios", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TipoFormularios",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Tipo = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TipoFormularios", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "UsuarioProfissoes",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Profissao = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Userid = table.Column<Guid>(type: "uniqueidentifier", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UsuarioProfissoes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "IndicadorTipos",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nome = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    Descricao = table.Column<string>(type: "nvarchar(600)", maxLength: 600, nullable: false),
                    UrlMetodologia = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    Slug = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true),
                    PeriodoId = table.Column<int>(type: "int", nullable: false),
                    IndicadorId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IndicadorTipos", x => x.Id);
                    table.ForeignKey(
                        name: "FK_IndicadorTipos_Indicadores_IndicadorId",
                        column: x => x.IndicadorId,
                        principalTable: "Indicadores",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "TipoRelatorios",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nome = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Slug = table.Column<string>(type: "nvarchar(120)", maxLength: 120, nullable: true),
                    SessaoId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TipoRelatorios", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TipoRelatorios_Sessaos_SessaoId",
                        column: x => x.SessaoId,
                        principalTable: "Sessaos",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "Posts",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nome = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    NomeCategoria = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    IdAutor = table.Column<int>(type: "int", nullable: false),
                    Descricao = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DescricaoCurta = table.Column<string>(type: "nvarchar(300)", maxLength: 300, nullable: true),
                    Status = table.Column<string>(type: "nvarchar(10)", maxLength: 10, nullable: true),
                    Slug = table.Column<string>(type: "nvarchar(120)", maxLength: 120, nullable: true),
                    IdImagemDestaque = table.Column<string>(type: "nvarchar(300)", maxLength: 300, nullable: true),
                    DataCriacao = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DataModificacao = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CategoriaId = table.Column<int>(type: "int", nullable: false),
                    SeoGoogleId = table.Column<int>(type: "int", nullable: false),
                    SeoRedesSociaisId = table.Column<int>(type: "int", nullable: false),
                    Discriminator = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    DataDoEvento = table.Column<DateTime>(type: "datetime2", nullable: true),
                    TipoDeApoioId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    TipoDeApoioId1 = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Posts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Posts_Categorias_CategoriaId",
                        column: x => x.CategoriaId,
                        principalTable: "Categorias",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Posts_SeoGoogles_SeoGoogleId",
                        column: x => x.SeoGoogleId,
                        principalTable: "SeoGoogles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Posts_SeoRedesSociais_SeoRedesSociaisId",
                        column: x => x.SeoRedesSociaisId,
                        principalTable: "SeoRedesSociais",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Posts_TipoDeApoios_TipoDeApoioId1",
                        column: x => x.TipoDeApoioId1,
                        principalTable: "TipoDeApoios",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "GravaFormularios",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DataCadastro = table.Column<DateTime>(type: "datetime2", nullable: true),
                    MensagemJson = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TipoFormularioId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GravaFormularios", x => x.Id);
                    table.ForeignKey(
                        name: "FK_GravaFormularios_TipoFormularios_TipoFormularioId",
                        column: x => x.TipoFormularioId,
                        principalTable: "TipoFormularios",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "Periodos",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nome = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Slug = table.Column<string>(type: "nvarchar(120)", maxLength: 120, nullable: true),
                    IndicadorTipoId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Periodos", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Periodos_IndicadorTipos_IndicadorTipoId",
                        column: x => x.IndicadorTipoId,
                        principalTable: "IndicadorTipos",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "GaleriaImagens",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UrlImagemG = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    UrlImagemM = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    UrlImagemP = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    PostId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GaleriaImagens", x => x.Id);
                    table.ForeignKey(
                        name: "FK_GaleriaImagens_Posts_PostId",
                        column: x => x.PostId,
                        principalTable: "Posts",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "PodCasts",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FramePodCast = table.Column<string>(type: "nvarchar(300)", maxLength: 300, nullable: false),
                    PostId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PodCasts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PodCasts_Posts_PostId",
                        column: x => x.PostId,
                        principalTable: "Posts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AssociaSeGalerias",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    AssociaSeId = table.Column<int>(type: "int", nullable: false),
                    GaleriaImagemId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AssociaSeGalerias", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AssociaSeGalerias_Associes_AssociaSeId",
                        column: x => x.AssociaSeId,
                        principalTable: "Associes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AssociaSeGalerias_GaleriaImagens_GaleriaImagemId",
                        column: x => x.GaleriaImagemId,
                        principalTable: "GaleriaImagens",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AssociaSePrincipaisAtividadesGalerias",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    AssociaSePrincipaisAtividadesId = table.Column<int>(type: "int", nullable: false),
                    GaleriaImagemId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AssociaSePrincipaisAtividadesGalerias", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AssociaSePrincipaisAtividadesGalerias_AssociaSePrincipaisAtividades_AssociaSePrincipaisAtividadesId",
                        column: x => x.AssociaSePrincipaisAtividadesId,
                        principalTable: "AssociaSePrincipaisAtividades",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AssociaSePrincipaisAtividadesGalerias_GaleriaImagens_GaleriaImagemId",
                        column: x => x.GaleriaImagemId,
                        principalTable: "GaleriaImagens",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AutorArtigos",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Slug = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    GaleriaImagemId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AutorArtigos", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AutorArtigos_GaleriaImagens_GaleriaImagemId",
                        column: x => x.GaleriaImagemId,
                        principalTable: "GaleriaImagens",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "GestaoGaleria",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    GestaoId = table.Column<int>(type: "int", nullable: false),
                    GaleriaImagemId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GestaoGaleria", x => x.Id);
                    table.ForeignKey(
                        name: "FK_GestaoGaleria_GaleriaImagens_GaleriaImagemId",
                        column: x => x.GaleriaImagemId,
                        principalTable: "GaleriaImagens",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_GestaoGaleria_Gestaos_GestaoId",
                        column: x => x.GestaoId,
                        principalTable: "Gestaos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PostGalerias",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PostId = table.Column<int>(type: "int", nullable: false),
                    GaleriaImagemId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PostGalerias", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PostGalerias_GaleriaImagens_GaleriaImagemId",
                        column: x => x.GaleriaImagemId,
                        principalTable: "GaleriaImagens",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PostGalerias_Posts_PostId",
                        column: x => x.PostId,
                        principalTable: "Posts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BannerGalerias",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    BannerId = table.Column<int>(type: "int", nullable: false),
                    GaleriaImagemId = table.Column<int>(type: "int", nullable: false),
                    Discriminator = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BannerGalerias", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BannerGalerias_GaleriaImagens_GaleriaImagemId",
                        column: x => x.GaleriaImagemId,
                        principalTable: "GaleriaImagens",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BannerPromocionals",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Status = table.Column<string>(type: "nvarchar(10)", maxLength: 10, nullable: false),
                    LinkDoBanner = table.Column<string>(type: "nvarchar(300)", maxLength: 300, nullable: false),
                    BannerGaleriaId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BannerPromocionals", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BannerPromocionals_BannerGalerias_BannerGaleriaId",
                        column: x => x.BannerGaleriaId,
                        principalTable: "BannerGalerias",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "BannerPromocionalGalerias",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    BannerPromocionalId = table.Column<int>(type: "int", nullable: false),
                    GaleriaImagemId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BannerPromocionalGalerias", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BannerPromocionalGalerias_BannerPromocionals_BannerPromocionalId",
                        column: x => x.BannerPromocionalId,
                        principalTable: "BannerPromocionals",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BannerPromocionalGalerias_GaleriaImagens_GaleriaImagemId",
                        column: x => x.GaleriaImagemId,
                        principalTable: "GaleriaImagens",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "HeroSections",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Description = table.Column<string>(type: "nvarchar(300)", maxLength: 300, nullable: false),
                    Backgroud = table.Column<string>(type: "nvarchar(300)", maxLength: 300, nullable: false),
                    BannerPromocionalGaleriaId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HeroSections", x => x.Id);
                    table.ForeignKey(
                        name: "FK_HeroSections_BannerPromocionalGalerias_BannerPromocionalGaleriaId",
                        column: x => x.BannerPromocionalGaleriaId,
                        principalTable: "BannerPromocionalGalerias",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateIndex(
                name: "IX_AssociaSeGalerias_AssociaSeId",
                table: "AssociaSeGalerias",
                column: "AssociaSeId");

            migrationBuilder.CreateIndex(
                name: "IX_AssociaSeGalerias_GaleriaImagemId",
                table: "AssociaSeGalerias",
                column: "GaleriaImagemId");

            migrationBuilder.CreateIndex(
                name: "IX_AssociaSePrincipaisAtividadesGalerias_AssociaSePrincipaisAtividadesId",
                table: "AssociaSePrincipaisAtividadesGalerias",
                column: "AssociaSePrincipaisAtividadesId");

            migrationBuilder.CreateIndex(
                name: "IX_AssociaSePrincipaisAtividadesGalerias_GaleriaImagemId",
                table: "AssociaSePrincipaisAtividadesGalerias",
                column: "GaleriaImagemId");

            migrationBuilder.CreateIndex(
                name: "IX_AutorArtigos_GaleriaImagemId",
                table: "AutorArtigos",
                column: "GaleriaImagemId");

            migrationBuilder.CreateIndex(
                name: "IX_BannerGalerias_BannerId",
                table: "BannerGalerias",
                column: "BannerId");

            migrationBuilder.CreateIndex(
                name: "IX_BannerGalerias_GaleriaImagemId",
                table: "BannerGalerias",
                column: "GaleriaImagemId");

            migrationBuilder.CreateIndex(
                name: "IX_BannerPromocionalGalerias_BannerPromocionalId",
                table: "BannerPromocionalGalerias",
                column: "BannerPromocionalId");

            migrationBuilder.CreateIndex(
                name: "IX_BannerPromocionalGalerias_GaleriaImagemId",
                table: "BannerPromocionalGalerias",
                column: "GaleriaImagemId");

            migrationBuilder.CreateIndex(
                name: "IX_BannerPromocionals_BannerGaleriaId",
                table: "BannerPromocionals",
                column: "BannerGaleriaId");

            migrationBuilder.CreateIndex(
                name: "IX_GaleriaImagens_PostId",
                table: "GaleriaImagens",
                column: "PostId");

            migrationBuilder.CreateIndex(
                name: "IX_GestaoGaleria_GaleriaImagemId",
                table: "GestaoGaleria",
                column: "GaleriaImagemId");

            migrationBuilder.CreateIndex(
                name: "IX_GestaoGaleria_GestaoId",
                table: "GestaoGaleria",
                column: "GestaoId");

            migrationBuilder.CreateIndex(
                name: "IX_GravaFormularios_TipoFormularioId",
                table: "GravaFormularios",
                column: "TipoFormularioId");

            migrationBuilder.CreateIndex(
                name: "IX_HeroSections_BannerPromocionalGaleriaId",
                table: "HeroSections",
                column: "BannerPromocionalGaleriaId");

            migrationBuilder.CreateIndex(
                name: "IX_IndicadorTipos_IndicadorId",
                table: "IndicadorTipos",
                column: "IndicadorId");

            migrationBuilder.CreateIndex(
                name: "IX_Periodos_IndicadorTipoId",
                table: "Periodos",
                column: "IndicadorTipoId");

            migrationBuilder.CreateIndex(
                name: "IX_PodCasts_PostId",
                table: "PodCasts",
                column: "PostId");

            migrationBuilder.CreateIndex(
                name: "IX_PostGalerias_GaleriaImagemId",
                table: "PostGalerias",
                column: "GaleriaImagemId");

            migrationBuilder.CreateIndex(
                name: "IX_PostGalerias_PostId",
                table: "PostGalerias",
                column: "PostId");

            migrationBuilder.CreateIndex(
                name: "IX_Posts_CategoriaId",
                table: "Posts",
                column: "CategoriaId");

            migrationBuilder.CreateIndex(
                name: "IX_Posts_SeoGoogleId",
                table: "Posts",
                column: "SeoGoogleId");

            migrationBuilder.CreateIndex(
                name: "IX_Posts_SeoRedesSociaisId",
                table: "Posts",
                column: "SeoRedesSociaisId");

            migrationBuilder.CreateIndex(
                name: "IX_Posts_TipoDeApoioId1",
                table: "Posts",
                column: "TipoDeApoioId1");

            migrationBuilder.CreateIndex(
                name: "IX_TipoRelatorios_SessaoId",
                table: "TipoRelatorios",
                column: "SessaoId");

            migrationBuilder.AddForeignKey(
                name: "FK_BannerGalerias_HeroSections_BannerId",
                table: "BannerGalerias",
                column: "BannerId",
                principalTable: "HeroSections",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BannerGalerias_HeroSections_BannerId",
                table: "BannerGalerias");

            migrationBuilder.DropTable(
                name: "Administradores");

            migrationBuilder.DropTable(
                name: "Associadas");

            migrationBuilder.DropTable(
                name: "AssociaSeGalerias");

            migrationBuilder.DropTable(
                name: "AssociaSePrincipaisAtividadesGalerias");

            migrationBuilder.DropTable(
                name: "AutorArtigos");

            migrationBuilder.DropTable(
                name: "Comites");

            migrationBuilder.DropTable(
                name: "GestaoGaleria");

            migrationBuilder.DropTable(
                name: "GravaFormularios");

            migrationBuilder.DropTable(
                name: "IpUsuario");

            migrationBuilder.DropTable(
                name: "LGPDS");

            migrationBuilder.DropTable(
                name: "Menus");

            migrationBuilder.DropTable(
                name: "Paginacoes");

            migrationBuilder.DropTable(
                name: "Periodos");

            migrationBuilder.DropTable(
                name: "PodCasts");

            migrationBuilder.DropTable(
                name: "PostGalerias");

            migrationBuilder.DropTable(
                name: "QuemSomosP");

            migrationBuilder.DropTable(
                name: "ServidorEmails");

            migrationBuilder.DropTable(
                name: "TipoRelatorios");

            migrationBuilder.DropTable(
                name: "UsuarioProfissoes");

            migrationBuilder.DropTable(
                name: "Associes");

            migrationBuilder.DropTable(
                name: "AssociaSePrincipaisAtividades");

            migrationBuilder.DropTable(
                name: "Gestaos");

            migrationBuilder.DropTable(
                name: "TipoFormularios");

            migrationBuilder.DropTable(
                name: "IndicadorTipos");

            migrationBuilder.DropTable(
                name: "Sessaos");

            migrationBuilder.DropTable(
                name: "Indicadores");

            migrationBuilder.DropTable(
                name: "HeroSections");

            migrationBuilder.DropTable(
                name: "BannerPromocionalGalerias");

            migrationBuilder.DropTable(
                name: "BannerPromocionals");

            migrationBuilder.DropTable(
                name: "BannerGalerias");

            migrationBuilder.DropTable(
                name: "GaleriaImagens");

            migrationBuilder.DropTable(
                name: "Posts");

            migrationBuilder.DropTable(
                name: "Categorias");

            migrationBuilder.DropTable(
                name: "SeoGoogles");

            migrationBuilder.DropTable(
                name: "SeoRedesSociais");

            migrationBuilder.DropTable(
                name: "TipoDeApoios");
        }
    }
}

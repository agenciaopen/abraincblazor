﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Abrainc.Data.Migrations
{
    public partial class bd1112 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Posts_TipoDeApoios_TipoDeApoioId1",
                table: "Posts");

            migrationBuilder.DropIndex(
                name: "IX_Posts_TipoDeApoioId1",
                table: "Posts");

            migrationBuilder.DropColumn(
                name: "DataDoEvento",
                table: "Posts");

            migrationBuilder.DropColumn(
                name: "Discriminator",
                table: "Posts");

            migrationBuilder.DropColumn(
                name: "TipoDeApoioId",
                table: "Posts");

            migrationBuilder.DropColumn(
                name: "TipoDeApoioId1",
                table: "Posts");

            migrationBuilder.AddColumn<int>(
                name: "PostId",
                table: "TipoDeApoios",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_TipoDeApoios_PostId",
                table: "TipoDeApoios",
                column: "PostId");

            migrationBuilder.AddForeignKey(
                name: "FK_TipoDeApoios_Posts_PostId",
                table: "TipoDeApoios",
                column: "PostId",
                principalTable: "Posts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TipoDeApoios_Posts_PostId",
                table: "TipoDeApoios");

            migrationBuilder.DropIndex(
                name: "IX_TipoDeApoios_PostId",
                table: "TipoDeApoios");

            migrationBuilder.DropColumn(
                name: "PostId",
                table: "TipoDeApoios");

            migrationBuilder.AddColumn<DateTime>(
                name: "DataDoEvento",
                table: "Posts",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Discriminator",
                table: "Posts",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<Guid>(
                name: "TipoDeApoioId",
                table: "Posts",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TipoDeApoioId1",
                table: "Posts",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Posts_TipoDeApoioId1",
                table: "Posts",
                column: "TipoDeApoioId1");

            migrationBuilder.AddForeignKey(
                name: "FK_Posts_TipoDeApoios_TipoDeApoioId1",
                table: "Posts",
                column: "TipoDeApoioId1",
                principalTable: "TipoDeApoios",
                principalColumn: "Id");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abrainc.Dominio.Model.Formulario
{
    /// <summary>
    /// FormContato / 
    /// </summary>
    public class TipoFormulario
    {
        public int Id { get; set; }

        public string? Tipo { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abrainc.Dominio.Model.Formulario
{
    public class GravaFormulario
    {
        public int Id { get; set; }
        public DateTime? DataCadastro { get; set; }

        //[MaxLength(100)]
        public string? MensagemJson { get; set; }

        public TipoFormulario? TipoFormulario { get; set; }

    }
}

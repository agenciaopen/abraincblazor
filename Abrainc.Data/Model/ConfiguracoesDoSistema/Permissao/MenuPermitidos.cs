﻿using Abrainc.Dominio.Model.ConfiguracoesDoSistema;
using Abrainc.Dominio.Model.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abrainc.Dominio.Model.ConfiguracoesDoSistema.Permissao
{
    /// <summary>
    /// Faz o controle da permissao de cada usuario no sistema, um usuário pode ter permissão a varios menus.
    /// </summary>
    public class MenuPermitidos
    {
        public Guid RoleId { get; set; }
        public Role? Role { get; set; }

        public int MenuId { get; set; }
        public ICollection<Menu>? Menu { get; set; }


    }
}

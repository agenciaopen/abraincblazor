﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abrainc.Dominio.Model.ConfiguracoesDoSistema
{
    /// <summary>
    /// Informações de SMTP para ser usado nos envio de email
    /// </summary>
    public class ServidorEmail
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "O campo de host é obrigatório")]
        [MaxLength(300)]
        public string? Host { get; set; }

        [Required(ErrorMessage = "O campo de senha é obrigatório")]
        [MaxLength(20)]
        public string? Passaword { get; set; }

        [Required(ErrorMessage = "O campo de email é obrigatório")]
        [MaxLength(100)]
        [EmailAddress(ErrorMessage = "Email com formato incorreto!")]
        public string? EmailSmtp { get; set; }
        public int Porta { get; set; }

    }
}

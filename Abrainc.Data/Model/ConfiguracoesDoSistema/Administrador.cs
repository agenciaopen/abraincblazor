﻿using Abrainc.Dominio.Model.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abrainc.Dominio.Model.ConfiguracoesDoSistema
{
    /// <summary>
    /// Informações do administrador super admin do sistema
    /// </summary>
    public class Administrador : User
    {

    }
}

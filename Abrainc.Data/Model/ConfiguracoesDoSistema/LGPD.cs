﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abrainc.Dominio.Model.ConfiguracoesDoSistema
{
    public class LGPD
    {
        public int Id { get; set; }
        public string? Descricao { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abrainc.Dominio.Model.ConfiguracoesDoSistema
{
    /// <summary>
    /// Controle de itens a serem mostrados por artigos/noticias/busca/dados mercado
    /// </summary>
    public class Paginacao
    {
        public int Id { get; set; }
        public int QuantidadeArtigos { get; set; } = 4;

        public int QuantidadeArtigosRelacionado { get; set; } = 3;
        public int QuantidadeNoticias { get; set; } = 4;

        public int QuantidadeNoticiasRelacionadas { get; set; } = 2;

        public int QuantidadeBusca { get; set; } = 4;
        public int QuantidadeSocial { get; set; } = 6;

        public int QuantidadeDadosMercado { get; set; } = 4;

        public int QuantidadeEventos { get; set; } = 4;
    }
}

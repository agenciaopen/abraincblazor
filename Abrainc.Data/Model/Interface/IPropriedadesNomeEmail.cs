﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abrainc.Dominio.Model.Interface
{
    public interface IPropriedadesNomeEmail
    {
        public int Id { get; set; }
        public string Nome { get; set; }

        public string Email { get; set; }
    }
}

﻿namespace Abrainc.Dominio.Model.Genericas
{
    public interface ICamposComuns
    {
        int Id { get; set; }
        string? Nome { get; set; }

        string? Slug { get; set; }
    }
}

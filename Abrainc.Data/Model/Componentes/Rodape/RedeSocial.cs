﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abrainc.Dominio.Model.Componentes.Rodape
{
    public class RedeSocial
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Campo nome é obrigatório!")]
        [MaxLength(100)]
        public string? Nome { get; set; }

        [Required(ErrorMessage = "Campo ícone é obrigatório!")]
        [MaxLength(100)]
        public string? UrlIcone { get; set; }

        [Required(ErrorMessage = "Campo url do link é obrigatório!")]
        [MaxLength(300)]
        [Url(ErrorMessage = "Url com formato incorreto")]
        public string? UrlLink { get; set; }


    }
}

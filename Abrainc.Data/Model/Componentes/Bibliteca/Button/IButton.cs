﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abrainc.Dominio.Model.Componentes.Bibliteca.Button
{
    public interface IButton
    {
        string Description { get; set; }
        string ClassButton { get; set; }
        string Link { get; set; }
    }
}

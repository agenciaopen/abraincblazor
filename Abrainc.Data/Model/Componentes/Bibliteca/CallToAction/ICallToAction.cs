﻿namespace Abrainc.Dominio.Model.Componentes.Bibliteca.CallToAction
{
    public interface ICallToAction
    {
        int Id { get; set; }

        string Title { get; set; }

        string Description { get; set; }

        string LeftToRightColumn { get; set; }



    }
}

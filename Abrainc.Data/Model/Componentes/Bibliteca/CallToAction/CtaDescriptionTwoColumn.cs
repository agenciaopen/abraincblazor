﻿using Abrainc.Dominio.Model.Componentes.Bibliteca.Background;
using Abrainc.Dominio.Model.Componentes.Bibliteca.Button;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abrainc.Dominio.Model.Componentes.Bibliteca.CallToAction
{

    public class CtaDescriptionTwoColumn : ICallToAction, IButton, IBackground
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "O campo de título é obrigatório")]
        [MaxLength(50)]
        public string? Title { get; set; }

        [Required(ErrorMessage = "A imagem de fundo é obrigatório")]
        [MaxLength(256)]
        public string? Backgroud { get; set; }

        [Required(ErrorMessage = "O campo de descrição é obrigatório")]
        [MaxLength(500)]
        public string? Description { get; set; }

        [MaxLength(20)]
        public string? LeftToRightColumn { get; set; }

        [MaxLength(50)]
        public string? ClassButton { get; set; }

        [MaxLength(256)]
        public string? Link { get; set; }
    }
}

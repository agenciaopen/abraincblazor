﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abrainc.Dominio.Model.Componentes.Bibliteca.SlideCarrocel
{
    public class Carrocel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "O campo da imagem é obrigatório")]
        [MaxLength(256)]
        public string? UrlImagem { get; set; }

        [Required(ErrorMessage = "O campo da de link é obrigatório")]
        [MaxLength(256)]
        [Url(ErrorMessage = "Url no formato inocorreto")]
        public string? Link { get; set; }
    }
}

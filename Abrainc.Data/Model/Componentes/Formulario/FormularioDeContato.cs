﻿namespace Abrainc.Dominio.Model.Componentes.Formulario
{
    /// <summary>
    /// Email que serão relacionado com cada setor
    /// </summary>
    public class FormularioDeContato : Setor
    {

        public int SetorId { get; set; }
        public Setor? Setor { get; set; }


    }
}

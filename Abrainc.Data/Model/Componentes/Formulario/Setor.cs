﻿using Abrainc.Dominio.Model.Interface;
using System.ComponentModel.DataAnnotations;

namespace Abrainc.Dominio.Model.Componentes.Formulario
{
    public class Setor : IPropriedadesNomeEmail
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "O campo nome do setor é obrigatório")]
        [MaxLength(100)]
        public string? Nome { get; set; }

        [Required(ErrorMessage = "O campo e-mail relacionado é obrigatório")]
        [MaxLength(100)]
        [EmailAddress(ErrorMessage = "E-mail no formato incorreto, favor verificar!")]
        public string? Email { get; set; }

    }
}

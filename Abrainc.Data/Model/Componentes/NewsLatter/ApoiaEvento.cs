﻿using Abrainc.Dominio.Model.Interface;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abrainc.Dominio.Model.Componentes.NewsLatter
{
    public class ApoiaEvento : IPropriedadesNomeEmail
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "O campo nome é obrigatório")]
        [MaxLength(100)]
        public string? Nome { get; set; }

        [Required(ErrorMessage = "O campo e-mail é obrigatório")]
        [MaxLength(100)]
        [EmailAddress(ErrorMessage = "E-mail no formato incorreto, favor verificar!")]
        public string? Email { get; set; }

        [Required(ErrorMessage = "O campo tipo de evento é obrigatório")]
        [MaxLength(100)]
        public string? TipoEvento { get; set; }
    }
}

﻿using Abrainc.Dominio.Model.Associadas;
using Abrainc.Dominio.Model.Galeria;

namespace Abrainc.Dominio.Model.Componentes.Associar
{
    public class AssociaSeGaleria : IGaleriaEntidade
    {
        public int Id { get; set; }
        public int AssociaSeId { get; set; }
        public virtual AssociaSe? AssociaSe { get; set; }

        public int GaleriaImagemId { get; set; }
        public virtual GaleriaImagem? GaleriaImagem { get; set; }
    }
}

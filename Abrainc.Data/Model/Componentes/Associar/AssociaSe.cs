﻿using System.ComponentModel.DataAnnotations;

namespace Abrainc.Dominio.Model.Componentes.Associar
{
    public class Associe
    {
        [Required(ErrorMessage = "O campo de descrição é obrigatório")]
        [MaxLength(100)]
        public string? Descricao { get; set; }

        [Required(ErrorMessage = "O campo de nome é obrigatório")]
        [MaxLength(300)]
        public string? LinkFormularioCadastro { get; set; }

        public virtual ICollection<AssociaSeGaleria>? AssociaSeGaleria { get; set; }
    }
}

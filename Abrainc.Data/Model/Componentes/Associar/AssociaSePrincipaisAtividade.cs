﻿using System.ComponentModel.DataAnnotations;

namespace Abrainc.Dominio.Model.Componentes.Associar
{
    public class AssociaSePrincipaisAtividade
    {
        public int Id { get; set; }

        [MaxLength(300)]
        public string? LinkParaRedirecionar { get; set; }

        public virtual ICollection<AssociaSePrincipaisAtividadesGaleria>? AssociaSePrincipaisAtividadesGaleria { get; set; }
    }
}

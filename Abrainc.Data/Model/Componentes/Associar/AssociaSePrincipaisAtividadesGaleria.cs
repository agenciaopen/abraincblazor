﻿using Abrainc.Dominio.Model.Galeria;

namespace Abrainc.Dominio.Model.Componentes.Associar
{
    public class AssociaSePrincipaisAtividadesGaleria : IGaleriaEntidade
    {
        public int Id { get; set; }
        public int AssociaSePrincipaisAtividadesId { get; set; }
        public virtual AssociaSePrincipaisAtividade? AssociaSePrincipaisAtividades { get; set; }

        public int GaleriaImagemId { get; set; }
        public virtual GaleriaImagem? GaleriaImagem { get; set; }


    }
}

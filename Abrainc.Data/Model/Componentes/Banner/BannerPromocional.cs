﻿using System.ComponentModel.DataAnnotations;

namespace Abrainc.Dominio.Model.Componentes.Banner
{
    public class BannerPromocional
    {
        public int Id { get; set; }


        [MaxLength(10)]
        public string Status { get; set; } = "Ativo";

        [Required(ErrorMessage = "O link do banner é obrigatório")]
        [MaxLength(300)]
        public string? LinkDoBanner { get; set; }

        public virtual BannerGaleria? BannerGaleria { get; set; }
    }
}

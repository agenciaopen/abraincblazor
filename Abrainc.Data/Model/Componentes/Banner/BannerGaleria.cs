﻿using Abrainc.Dominio.Model.Galeria;

namespace Abrainc.Dominio.Model.Componentes.Banner
{
    /// <summary>
    /// Relacionamento das imagens do banner com a galeria
    /// </summary>
    public class BannerGaleria : IGaleriaEntidade
    {
        public int Id { get; set; }
        public int BannerId { get; set; }
        public virtual HeroSection? Banner { get; set; }

        public int GaleriaImagemId { get; set; }
        public virtual GaleriaImagem? GaleriaImagem { get; set; }
    }
}

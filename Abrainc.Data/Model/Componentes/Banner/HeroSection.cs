﻿
using Abrainc.Dominio.Model.Componentes.Bibliteca.Background;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abrainc.Dominio.Model.Componentes.Banner
{
    public class HeroSection : IHeroSection, IBackground
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "O campo título é obrigatório")]
        [MaxLength(100)]
        public string? Title { get; set; }

        [Required(ErrorMessage = "O campo descrição é obrigatório")]
        [MaxLength(300)]
        public string? Description { get; set; }

        [Required(ErrorMessage = "A imagem de background é obrigatório")]
        [MaxLength(300)]
        public string? Backgroud { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public virtual BannerPromocionalGaleria? BannerPromocionalGaleria { get; set; }


    }
}

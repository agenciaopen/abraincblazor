﻿using Abrainc.Dominio.Model.Galeria;

namespace Abrainc.Dominio.Model.Componentes.Banner
{
    /// <summary>
    /// Relacionamento das imagens do banner promocional com a galeria
    /// </summary>
    public class BannerPromocionalGaleria
    {
        public int Id { get; set; }
        public int BannerPromocionalId { get; set; }
        public virtual BannerPromocional? BannerPromocional { get; set; }

        public int GaleriaImagemId { get; set; }
        public virtual GaleriaImagem? GaleriaImagem { get; set; }
    }
}

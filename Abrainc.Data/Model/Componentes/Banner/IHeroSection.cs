﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abrainc.Dominio.Model.Componentes.Banner
{
    public interface IHeroSection
    {
        int Id { get; set; }

        string? Title { get; set; }
        string? Description { get; set; }
    }
}

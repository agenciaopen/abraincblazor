using Abrainc.Data.Model.Universidades;

namespace Abrainc.Data.Model.AbraincArtigo
{
    public class ArtigoAcademico
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public int UniversidadeId { get; set; }
        public virtual Universidade Universidade { get; set; }
        public virtual IList<Autor> Autores { get; set; } 
        public string Descricao { get; set; }
        public string UrlArtigoPdf { get; set; }
        public DateTime DataArtigoPublicado { get; set; }
    }
}
using Abrainc.Data.Model.Universidades;

namespace Abrainc.Data.Model.AbraincArtigo
{
    public class Autor
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string NivelGraduacao { get; set; }
        public int UniversidadeId { get; set; }
        public int ArtigoAcademicoId { get; set; }
        public virtual ArtigoAcademico ArtigoAcademico { get; set; }
        public virtual Universidade Universidade { get; set; }
    }
}
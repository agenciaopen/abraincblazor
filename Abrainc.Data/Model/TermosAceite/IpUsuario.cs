﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abrainc.Dominio.Model.TermosAceite
{
    /// <summary>
    /// Dados de acesso ao site para gravar no aceite dos termos
    /// </summary>
    public class IpUsuario
    {
        public int Id { get; set; }
        public string? Ip { get; set; }

        public DateTime DataRegistro { get; set; }
    }
}

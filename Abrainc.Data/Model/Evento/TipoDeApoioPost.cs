﻿using Abrainc.Dominio.Model.Posts;
using System.ComponentModel.DataAnnotations;

namespace Abrainc.Dominio.Model.Evento
{
    public class TipoDeApoioPost
    {
        public int Id { get; set; }

        public int PostId { get; set; }

        public Post? Posts { get; set; }

        [Required(ErrorMessage = "Campo de descrição é obrigatório!")]
        [MaxLength(256)]
        public string? Descricao { get; set; }


    }
}

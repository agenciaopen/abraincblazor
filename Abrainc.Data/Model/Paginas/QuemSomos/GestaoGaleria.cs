﻿using Abrainc.Dominio.Model.Galeria;

namespace Abrainc.Dominio.Model.Paginas.QuemSomos
{
    /// <summary>
    /// Relacionamento dos gestores e a galeria de imagem
    /// </summary>
    public class GestaoGaleria : IGaleriaEntidade
    {
        public int Id { get; set; }
        public int GestaoId { get; set; }
        public virtual Gestao? Gestao { get; set; }

        public int GaleriaImagemId { get; set; }
        public virtual GaleriaImagem? GaleriaImagem { get; set; }
    }
}

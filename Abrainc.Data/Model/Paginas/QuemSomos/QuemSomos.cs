﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abrainc.Dominio.Model.Paginas.QuemSomos
{
    public class QuemSomos
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "O campo de nome é obrigatório")]
        [MaxLength(20)]
        public string? Titulo { get; set; }

        [Required(ErrorMessage = "O campo de nome é obrigatório")]
        public string? Descricao { get; set; }

        #region PRINCIPIOS E VALORES
        [Required(ErrorMessage = "O campo de ícone é obrigatório")]
        public string? PrincipiosValoresIcone { get; set; }

        [Required(ErrorMessage = "O campo de nome é obrigatório")]
        public string? PrincipiosValoresDescricao { get; set; }
        #endregion


    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abrainc.Dominio.Model.Paginas.QuemSomos
{
    public class Comite
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "O campo de título é obrigatório")]
        [MaxLength(20)]
        public string? Titulo { get; set; }

        [Required(ErrorMessage = "O campo ícone é obrigatório")]
        [MaxLength(50)]
        public string? Icone { get; set; }
    }
}

﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abrainc.Dominio.Model.Paginas.QuemSomos
{
    /// <summary>
    ///  Gestores da empresa 
    /// </summary>
    public class Gestao
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "O campo de nome é obrigatório")]
        [MaxLength(50)]
        public string? Nome { get; set; }

        [Required(ErrorMessage = "O campo de nome é obrigatório")]
        [MaxLength(50)]
        public string? Cargo { get; set; }

        public virtual ICollection<GestaoGaleria>? GestaoGalerias { get; set; }
    }
}

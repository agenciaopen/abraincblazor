﻿namespace Abrainc.Dominio.Model.Seo
{
    public interface ISeo
    {
        int Id { get; set; }
        string Title { get; set; }
        string Description { get; set; }
        string Keyword { get; set; }
    }
}

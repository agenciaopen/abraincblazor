﻿using System.ComponentModel.DataAnnotations;

namespace Abrainc.Dominio.Model.Seo
{
    public class SeoRedesSociais : ISeo
    {
        public int Id { get; set; }

        [MaxLength(60)]
        public string? Title { get; set; }
        [MaxLength(350)]
        public string? Description { get; set; }
        [MaxLength(100)]
        public string? Keyword { get; set; }

        [MaxLength(250)]
        public string? Image { get; set; }

        [MaxLength(10)]
        public string? Type { get; set; } = "Page";

        //public int PostId { get; set; }

        //public Model.Posts.Post? Post { get; set; }

    }
}

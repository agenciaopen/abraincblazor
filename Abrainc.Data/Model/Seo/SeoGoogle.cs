﻿
using System.ComponentModel.DataAnnotations;

namespace Abrainc.Dominio.Model.Seo
{
    public class SeoGoogle : ISeo
    {
        public int Id { get; set; }

        [MaxLength(60)]
        public string? Title { get; set; }
        [MaxLength(350)]
        public string? Description { get; set; }
        [MaxLength(100)]
        public string? Keyword { get; set; }

        //public int PostId { get; set; }

        //public virtual Model.Posts.Post? Post { get; set; }



    }
}

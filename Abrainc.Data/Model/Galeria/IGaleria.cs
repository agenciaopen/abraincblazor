﻿namespace Abrainc.Dominio.Model.Galeria
{
    public interface IGaleria
    {
        int Id { get; set; }
        string? UrlImagemG { get; set; }
        string? UrlImagemM { get; set; }
        string? UrlImagemP { get; set; }
    }
}

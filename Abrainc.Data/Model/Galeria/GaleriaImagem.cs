﻿using System.ComponentModel.DataAnnotations;

namespace Abrainc.Dominio.Model.Galeria
{
    public class GaleriaImagem : IGaleria
    {
        public int Id { get; set; }

        [MaxLength(256)]
        public string? UrlImagemG { get; set; }

        [MaxLength(256)]
        public string? UrlImagemM { get; set; }

        [MaxLength(256)]
        public string? UrlImagemP { get; set; }


    }
}

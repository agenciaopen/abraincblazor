﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abrainc.Dominio.Model.Galeria
{
    public interface IGaleriaEntidade
    {
        int Id { get; set; }
        int GaleriaImagemId { get; set; }
        GaleriaImagem? GaleriaImagem { get; set; }

    }
}

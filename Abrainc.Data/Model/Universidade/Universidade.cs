namespace Abrainc.Data.Model.Universidades
{
    public class Universidade
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string UrlImagem { get; set; }
    }
}
﻿using System.ComponentModel.DataAnnotations;

namespace Abrainc.Dominio.Model.Identity
{
    public class Role
    {
        public Guid Id { get; set; }
        [Required(ErrorMessage = "O nome do perfil é obrigatório", AllowEmptyStrings = false)]

        [MaxLength(256)]
        public string Name { get; set; }
        [MaxLength(256)]
        public string NormalizedName { get; set; }
        public Guid ConcurrencyStamp { get; set; }
    }
}

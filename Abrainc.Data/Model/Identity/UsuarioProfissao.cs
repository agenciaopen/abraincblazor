﻿namespace Abrainc.Dominio.Model.Identity
{
    public class UsuarioProfissao
    {
        public int Id { get; set; }

        public string? Profissao { get; set; }

        public Guid? Userid { get; set; }


    }
}

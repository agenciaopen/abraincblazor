﻿using System.ComponentModel.DataAnnotations;

namespace Abrainc.Dominio.Model.Lab.Paginas
{
    /// <summary>
    /// Pagina interna de indicadores, onde um indicador tem um tipo
    /// </summary>
    public class IndicadorInterna : Posts.Post
    {


        [Required(ErrorMessage = "O arquivo de PDF é obrigatório")]
        [MaxLength(300)]
        public string? UrlPdf { get; set; }

        public int IndicadorId { get; set; }
        public Indicador? Indicador { get; set; }

        public int IndicadorTipoId { get; set; }
        public IndicadorTipo? IndicadorTipo { get; set; }


    }
}

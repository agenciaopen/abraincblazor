﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abrainc.Dominio.Model.Lab.Paginas
{
    /// <summary>
    /// 
    /// </summary>
    public class DadosMercadoInterna
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "O campo de descrição é obrigatório")]
        [MaxLength(300)]
        public string? Titulo { get; set; }

        [Required(ErrorMessage = "O campo de descrição é obrigatório")]
        [MaxLength(600)]
        public string? Descricao { get; set; }

        [Required(ErrorMessage = "O arquivo de PDF é obrigatório")]
        [MaxLength(300)]
        public string? UrlPdf { get; set; }
    }
}

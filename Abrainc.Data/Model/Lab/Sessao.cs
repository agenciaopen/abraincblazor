﻿using Abrainc.Dominio.Model.Genericas;
using System.ComponentModel.DataAnnotations;

namespace Abrainc.Dominio.Model.Lab
{
    /// <summary>
    /// O objetivo desta funcionalidade será sessões a serem utilizadas no cadastro dos indicadores
    /// Estudos / Indicadores / Relatorios
    /// </summary>
    public class Sessao : ICamposComuns
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Campo nome é obrigatório!")]
        [MaxLength(100)]
        public string? Nome { get; set; }

        [MaxLength(120)]
        public string? Slug { get; set; }

        public int TipoRelatorioId { get; set; }

        public virtual ICollection<TipoRelatorio>? TipoRelatorio { get; set; }
    }
}

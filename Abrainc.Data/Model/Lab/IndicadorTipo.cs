﻿using Abrainc.Dominio.Model.Genericas;
using System.ComponentModel.DataAnnotations;

namespace Abrainc.Dominio.Model.Lab
{
    /// <summary>
    /// O objetivo desta funcionalidade será sessões a serem utilizadas no cadastro dos indicadores
    /// Ex.: Mensal / IAMI / RADAR TRIMESTRAL/Mensal/IndiceConfianca
    /// </summary>
    public class IndicadorTipo : ICamposComuns
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "O campo de nome é obrigatório")]
        [MaxLength(50)]
        public string? Nome { get; set; }

        [Required(ErrorMessage = "O campo de descrição é obrigatório")]
        [MaxLength(600)]
        public string? Descricao { get; set; }

        [MaxLength(256)]
        public string? UrlMetodologia { get; set; }

        [MaxLength(150)]
        public string? Slug { get; set; }

        public int PeriodoId { get; set; }
        public virtual ICollection<Periodo>? Periodos { get; set; }
    }
}

﻿using Abrainc.Dominio.Model.Genericas;
using System.ComponentModel.DataAnnotations;

namespace Abrainc.Dominio.Model.Lab
{
    /// <summary>
    /// O objetivo desta funcionalidade sera cadastrar os tipos de indicador
    /// Ex.: Fipe/Delloite
    /// </summary>
    public class Indicador : ICamposComuns
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "O campo de nome é obrigatório")]
        [MaxLength(50)]
        public string? Nome { get; set; }

        [MaxLength(120)]
        public string? Slug { get; set; }

        [Required(ErrorMessage = "O campo da logo é obrigatório")]
        [MaxLength(300)]
        public string? UrlImagemLogo { get; set; }

        public int IndicadorTiposId { get; set; }
        public virtual ICollection<IndicadorTipo>? IndicadorTipos { get; set; }
    }
}

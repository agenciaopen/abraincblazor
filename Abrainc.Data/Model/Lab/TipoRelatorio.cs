﻿using Abrainc.Dominio.Model.Genericas;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abrainc.Dominio.Model.Lab
{
    /// <summary>
    ///  Rel.powerbi / Indicadores / Dados mercado
    /// </summary>
    public class TipoRelatorio : ICamposComuns
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Campo nome é obrigatório!")]
        [MaxLength(100)]
        public string? Nome { get; set; }

        [MaxLength(120)]
        public string? Slug { get; set; }
    }
}

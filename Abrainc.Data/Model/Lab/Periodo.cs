﻿using Abrainc.Dominio.Model.Genericas;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abrainc.Dominio.Model.Lab
{
    /// <summary>
    /// O objetivo desta funcionalidade será criar período que será utilizado no cadastro dos indicadores de acordo com a categoria selecionada.
    /// </summary>
    public class Periodo : ICamposComuns
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "O nome é obrigatório")]
        [MaxLength(100)]
        public string? Nome { get; set; }

        [MaxLength(120)]
        public string? Slug { get; set; }
    }
}

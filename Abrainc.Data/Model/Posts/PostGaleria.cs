﻿using Abrainc.Dominio.Model.Galeria;

namespace Abrainc.Dominio.Model.Posts
{
    public class PostGaleria : IGaleriaEntidade
    {
        public int Id { get; set; }
        public int PostId { get; set; }
        public  Post? Post { get; set; }

        public int GaleriaImagemId { get; set; }
        public GaleriaImagem? GaleriaImagem { get; set; }
    }
}

﻿using Abrainc.Dominio.Model.Genericas;
using System.ComponentModel.DataAnnotations;

namespace Abrainc.Dominio.Model.Posts
{
    public class Categoria : ICamposComuns
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Campo nome é obrigatório!")]
        [MaxLength(100)]
        public string? Nome { get; set; }

        [Required(ErrorMessage = "Por favor, escolha um ícone para esta categoria!")]
        [MaxLength(100)]
        public string? IconeDaCategoria { get; }

        [MaxLength(120)]
        public string? Slug { get; set; }

        public int PostId { get; set; }
        public virtual ICollection<Post>? Posts { get; set; }
    }
}

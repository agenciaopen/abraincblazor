﻿using System.ComponentModel.DataAnnotations;

namespace Abrainc.Dominio.Model.Posts
{
    /// <summary>
    /// Os podcast serão guardados com seu IFrame e relacioando com uma PostId
    /// </summary>
    public class PodCast
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "O campo Frame é obrigatório, por favor cole o código no campo.")]
        [MaxLength(300)]
        public string? FramePodCast { get; set; }

        public int PostId { get; set; }

        public Post? Post { get; set; }

    }
}


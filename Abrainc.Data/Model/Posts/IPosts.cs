﻿namespace Abrainc.Dominio.Model.Posts
{
    public interface IPosts
    {
        int Id { get; set; }

        string Nome { get; set; }

        string NomeCategoria { get; set; }

        int IdAutor { get; set; }

        string Descricao { get; set; }

        string DescricaoCurta { get; set; }

        string Status { get; set; }

        string Slug { get; set; }

        string IdImagemDestaque { get; set; }

        DateTime DataCriacao { get; set; }

        DateTime DataModificacao { get; set; }
    }
}

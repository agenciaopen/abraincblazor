﻿using Abrainc.Dominio.Model.Galeria;
using Abrainc.Dominio.Model.Seo;
using System.ComponentModel.DataAnnotations;

namespace Abrainc.Dominio.Model.Posts
{
    public class Post : IPosts
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "O campo nome é obrigatório")]
        [MaxLength(100)]
        public string? Nome { get; set; }

        [Required(ErrorMessage = "Categoria é obrigatória")]
        [MaxLength(50)]
        public string? NomeCategoria { get; set; }
        public int IdAutor { get; set; }

        public string? Descricao { get; set; }

        [MaxLength(300)]
        public string? DescricaoCurta { get; set; }

        [MaxLength(10)]
        public string? Status { get; set; } = "Ativo";

        [MaxLength(120)]
        public string? Slug { get; set; }

        [MaxLength(300)]
        public string? IdImagemDestaque { get; set; }
        public DateTime DataCriacao { get; set; } = DateTime.Now;
        public DateTime DataModificacao { get; set; } = DateTime.Now;


        public int CategoriaId { get; set; }
        public virtual Categoria? Categoria { get; set; }

        public int SeoGoogleId { get; set; }
        public virtual SeoGoogle? SeoGoogle { get; set; }

        public int SeoRedesSociaisId { get; set; }
        public virtual SeoRedesSociais? SeoRedesSociais { get; set; }

        public virtual ICollection<GaleriaImagem>? GaleriaImagens { get; set; }




    }
}

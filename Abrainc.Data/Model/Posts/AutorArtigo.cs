﻿using Abrainc.Dominio.Model.Galeria;

namespace Abrainc.Dominio.Model.Posts;

public class AutorArtigo
{
    public int Id { get; set; }
    public string? Slug { get; set; }

    //public AutorArtigo(int id , string nome)
    //{
    //    if(Id > 0 && ! string.IsNullOrEmpty(nome))
    //    {
    //        throw new Exception("Favor verificar o Id e o Nome do autor");
    //    }

    //    Id= id;
    //    Slug = MetodosExtensao.GeradorDeSlug.CriaSlug(nome);
    //}    

    public int GaleriaImagemId { get; set; }
    public virtual GaleriaImagem? GaleriaImagem { get; set; }
}

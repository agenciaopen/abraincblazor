﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abrainc.Dominio.Model.Associadas
{
    public class AssociaSe
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "O campo de incorporadora é obrigatório")]
        [MaxLength(300)]
        public string? Incorporadora { get; set; }

        [Required(ErrorMessage = "O campo de classificação da incorporadora é obrigatório")]
        public int ClassificacaoIncorporadora { get; set; }

        [Required(ErrorMessage = "A região de atuação é obrigatório")]
        [MaxLength(20)]
        public string? RegiaoAtuacao { get; set; }

        [Required(ErrorMessage = "O campo de número de lancamentos é obrigatório")]
        public int NumeroDeLancamentos { get; set; }

        [Required(ErrorMessage = "O campo de número de unidades é obrigatório")]
        public int NumeroDeUnidadesEntregue { get; set; }

        [Required(ErrorMessage = "A campo responsável é obrigatório")]
        [MaxLength(50)]
        public string? Responsavel { get; set; }

        [Required(ErrorMessage = "A campo de cargo é obrigatório")]
        [MaxLength(20)]
        public string? Cargo { get; set; }

        [Required(ErrorMessage = "Telefone é obrigatório")]
        [MaxLength(20)]
        public string? Telefone { get; set; }

        [Required(ErrorMessage = "O e-mail é obrigatório")]
        [MaxLength(100)]
        [EmailAddress(ErrorMessage = "E-mail inválido!")]
        public string? Email { get; set; }

        [Required(ErrorMessage = "A mensagem é obrigatório")]
        [MaxLength(300)]
        public string? Mensagem { get; set; }


    }
}

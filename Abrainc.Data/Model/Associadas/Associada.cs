﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abrainc.Dominio.Model.Associadas
{
    /// <summary>
    /// Neste classe deve escolher o tipo de associada, seja ela de apoio institucional ou associada, estão relacionadas nos Enums
    /// </summary>
    public class Associada
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "O campo nome é obrigatório")]
        [MaxLength(50)]
        public string? Nome { get; set; }

        [Required(ErrorMessage = "O campo da logo é obrigatório")]
        [MaxLength(256)]
        public string? UrlDaLogo { get; set; }

        [MaxLength(256)]
        public string? LinkExterno { get; set; }

        [Required(ErrorMessage = "Por favor, escolha o tipo!")]
        public int EnumRTipoAssociada { get; set; }

        [Required(ErrorMessage = "Por favor, escolha a região da associada!")]
        public int EnumRegiao { get; set; }


    }
}

﻿
using Abrainc.Dominio.Model.Componentes.Banner;

namespace Abrainc.Dominio.Model.AbraincSocial
{
    /// <summary>
    /// Relacionamento da imagens que irão aparecer no topo dos posts de abrainc social
    /// </summary>
    public class LogosTopo : BannerGaleria
    {
        //public int Id { get; set; }
    }
}
